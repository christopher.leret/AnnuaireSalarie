﻿namespace annuairecorp
{
    partial class emplacementBureau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCoordPersonnel = new System.Windows.Forms.Label();
            this.lblMetierPersonnel = new System.Windows.Forms.Label();
            this.lblIdentitePersonnel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblCoordPersonnel);
            this.panel1.Controls.Add(this.lblMetierPersonnel);
            this.panel1.Controls.Add(this.lblIdentitePersonnel);
            this.panel1.Location = new System.Drawing.Point(221, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(271, 109);
            this.panel1.TabIndex = 5;
            // 
            // lblCoordPersonnel
            // 
            this.lblCoordPersonnel.AutoSize = true;
            this.lblCoordPersonnel.Location = new System.Drawing.Point(4, 28);
            this.lblCoordPersonnel.Name = "lblCoordPersonnel";
            this.lblCoordPersonnel.Size = new System.Drawing.Size(16, 13);
            this.lblCoordPersonnel.TabIndex = 6;
            this.lblCoordPersonnel.Text = "...";
            // 
            // lblMetierPersonnel
            // 
            this.lblMetierPersonnel.AutoSize = true;
            this.lblMetierPersonnel.Location = new System.Drawing.Point(4, 61);
            this.lblMetierPersonnel.Name = "lblMetierPersonnel";
            this.lblMetierPersonnel.Size = new System.Drawing.Size(16, 13);
            this.lblMetierPersonnel.TabIndex = 1;
            this.lblMetierPersonnel.Text = "...";
            // 
            // lblIdentitePersonnel
            // 
            this.lblIdentitePersonnel.AutoSize = true;
            this.lblIdentitePersonnel.Location = new System.Drawing.Point(4, 7);
            this.lblIdentitePersonnel.Name = "lblIdentitePersonnel";
            this.lblIdentitePersonnel.Size = new System.Drawing.Size(16, 13);
            this.lblIdentitePersonnel.TabIndex = 0;
            this.lblIdentitePersonnel.Text = "...";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(31, 135);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(302, 245);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // emplacementBureau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(507, 398);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Name = "emplacementBureau";
            this.Text = "Emplacement du Bureau";
            this.Load += new System.EventHandler(this.emplacementBureau_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCoordPersonnel;
        private System.Windows.Forms.Label lblMetierPersonnel;
        private System.Windows.Forms.Label lblIdentitePersonnel;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}