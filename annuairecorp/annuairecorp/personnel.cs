﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace annuairecorp
{
    public class personnel
    {
        private int numero;
        private string civilite;
        private string nom;
        private string prenom;
        private string numteltravail;
        private string mailtravail;
        private string metier;
        private int coordX;
        private int coordY;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public string Civilite
        {
            get { return civilite; }
            set { civilite = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        public string NumTelTravail
        {
            get { return numteltravail; }
            set { numteltravail = value; }
        }

        public string MailTravail
        {
            get { return mailtravail; }
            set { mailtravail = value; }
        }

        public string Metier
        {
            get { return metier; }
            set { metier = value; }
        }

        public int CoordX
        {
            get { return coordX; }
            set { coordX = value; }
        }

        public int CoordY
        {
            get { return coordY; }
            set { coordY = value; }
        }

        public personnel()
        {
        }
    }
}
