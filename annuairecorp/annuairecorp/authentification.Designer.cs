﻿namespace annuairecorp
{
    partial class authentification
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuitter = new System.Windows.Forms.Button();
            this.btnValider = new System.Windows.Forms.Button();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblMdp = new System.Windows.Forms.Label();
            this.ttbLog = new System.Windows.Forms.TextBox();
            this.ttbMdp = new System.Windows.Forms.TextBox();
            this.lblErreurCo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnQuitter
            // 
            this.btnQuitter.BackColor = System.Drawing.Color.Beige;
            this.btnQuitter.Location = new System.Drawing.Point(20, 96);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(75, 23);
            this.btnQuitter.TabIndex = 0;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = false;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // btnValider
            // 
            this.btnValider.BackColor = System.Drawing.Color.Beige;
            this.btnValider.Location = new System.Drawing.Point(140, 95);
            this.btnValider.Name = "btnValider";
            this.btnValider.Size = new System.Drawing.Size(75, 23);
            this.btnValider.TabIndex = 1;
            this.btnValider.Text = "Valider";
            this.btnValider.UseVisualStyleBackColor = false;
            this.btnValider.Click += new System.EventHandler(this.btnValider_Click);
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(14, 14);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(33, 13);
            this.lblLogin.TabIndex = 2;
            this.lblLogin.Text = "Login";
            // 
            // lblMdp
            // 
            this.lblMdp.AutoSize = true;
            this.lblMdp.Location = new System.Drawing.Point(14, 44);
            this.lblMdp.Name = "lblMdp";
            this.lblMdp.Size = new System.Drawing.Size(71, 13);
            this.lblMdp.TabIndex = 3;
            this.lblMdp.Text = "Mot de passe";
            // 
            // ttbLog
            // 
            this.ttbLog.Location = new System.Drawing.Point(125, 11);
            this.ttbLog.Name = "ttbLog";
            this.ttbLog.Size = new System.Drawing.Size(100, 20);
            this.ttbLog.TabIndex = 4;
            // 
            // ttbMdp
            // 
            this.ttbMdp.Location = new System.Drawing.Point(125, 41);
            this.ttbMdp.Name = "ttbMdp";
            this.ttbMdp.PasswordChar = '*';
            this.ttbMdp.Size = new System.Drawing.Size(100, 20);
            this.ttbMdp.TabIndex = 5;
            // 
            // lblErreurCo
            // 
            this.lblErreurCo.AutoSize = true;
            this.lblErreurCo.Location = new System.Drawing.Point(53, 75);
            this.lblErreurCo.Name = "lblErreurCo";
            this.lblErreurCo.Size = new System.Drawing.Size(16, 13);
            this.lblErreurCo.TabIndex = 6;
            this.lblErreurCo.Text = "...";
            // 
            // authentification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(241, 130);
            this.ControlBox = false;
            this.Controls.Add(this.lblErreurCo);
            this.Controls.Add(this.ttbMdp);
            this.Controls.Add(this.ttbLog);
            this.Controls.Add(this.lblMdp);
            this.Controls.Add(this.lblLogin);
            this.Controls.Add(this.btnValider);
            this.Controls.Add(this.btnQuitter);
            this.Name = "authentification";
            this.Text = "Authentification";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Button btnValider;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblMdp;
        private System.Windows.Forms.TextBox ttbLog;
        private System.Windows.Forms.TextBox ttbMdp;
        private System.Windows.Forms.Label lblErreurCo;
    }
}

