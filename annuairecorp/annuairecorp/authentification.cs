﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace annuairecorp
{
    public partial class authentification : Form
    {
        public authentification()
        {
            InitializeComponent();
            lblErreurCo.Text = "";
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnValider_Click(object sender, EventArgs e)
        {
            database database = new database(ttbLog.Text, ttbMdp.Text);
            if (database.validationConnexion() == 1)
            {
                recherchebureau FormRechBureau = new recherchebureau(database);
                this.Close();
                FormRechBureau.ShowDialog();
            }
            else
            {
                lblErreurCo.Text = "Impossible de se connecter";
                lblErreurCo.ForeColor = System.Drawing.Color.Red;
            }

        }
    }
}
