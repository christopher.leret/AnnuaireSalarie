﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace annuairecorp
{
    public class database
    {
        String login, motDePasse;
        String host = "80.82.238.198", port = "62543", dbase = "examen_epreuve_e4";
        String connect;

        public database()
        {
        }

        public database(String login, String motdepasse)
        {
            this.login = login;
            this.motDePasse = motdepasse;
        }

        public int validationConnexion()
        {
            int validationconnexion;
            using (var conn = new NpgsqlConnection("Host=" + host + ";Port=" + port + ";Username=" +
                login + ";Password=" + motDePasse + ";Database=" + dbase))
            {
                try
                {
                    conn.Open();
                    conn.Close();
                    validationconnexion = 1;
                    connect = "Host=" + host + ";Port=" + port + ";Username=" + login + ";Password=" +
                        motDePasse + ";Database=" + dbase;
                }
                catch
                {
                    validationconnexion = 0;
                }
            }
            return validationconnexion;
        }

        public List<personnel> listePersonnelParPremiereLettre(String lettre)
        {
            List<personnel> listePersonnel = new List<personnel>();
            using (var conn = new NpgsqlConnection(connect))
            {
                using (var cmd = new NpgsqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "SET SCHEMA 'annuaireCorp';" +
                        "SELECT numero,civilite, nom, prenom, numteltravail,mailtravail,"+
                        "nummetier,coordx,coordy FROM personnel " +
                        "WHERE nom LIKE '" + lettre + "%' " +
                        "OR prenom LIKE '" + lettre + "%';";
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            personnel personnelActuel = new personnel();
                            personnelActuel.Numero = reader.GetInt32(0);
                            personnelActuel.Civilite = reader.GetString(1);
                            personnelActuel.Nom = reader.GetString(2);
                            personnelActuel.Prenom = reader.GetString(3);
                            personnelActuel.NumTelTravail = reader.GetString(4);
                            personnelActuel.MailTravail = reader.GetString(5);
                            personnelActuel.Metier = emploieDuPersonnelParNum(reader.GetInt32(6));
                            personnelActuel.CoordX = reader.GetInt32(7);
                            personnelActuel.CoordY = reader.GetInt32(8);
                            listePersonnel.Add(personnelActuel);
                        }
                    }
                    conn.Close();
                }
            }
            return listePersonnel;
        }

        public string emploieDuPersonnelParNum(int numeroMetier)
        {
            string emploi = null;
            using (var conn = new NpgsqlConnection(connect))
            {
                using (var cmd = new NpgsqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "SET SCHEMA 'annuaireCorp';" +
                        "SELECT nom FROM metier WHERE numero =" + numeroMetier;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            emploi = reader.GetString(0);
                        }
                    }
                    conn.Close();
                }
            }
            return emploi;
        }

        public string coordonneeBatiment(personnel personnel)
        {
            string coord = null;
            using (var conn = new NpgsqlConnection(connect))
            {
                using (var cmd = new NpgsqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "SET SCHEMA 'annuaireCorp';" +
                        "SELECT coordx,coordy FROM batiment WHERE code = (select codeBat from plan where numero = ("+
                        "Select numplan from personnel where numero ="+personnel.Numero+"))";
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            coord = reader.GetInt32(0).ToString() + "_" + reader.GetInt32(1).ToString();
                        }
                    }
                    conn.Close();
                }
            }
            return coord;
        }

        public string cheminPlan(personnel personnel)
        {
            string chemin = null;
            using (var conn = new NpgsqlConnection(connect))
            {
                using (var cmd = new NpgsqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "SET SCHEMA 'annuaireCorp';" +
                        "SELECT chemin FROM plan WHERE numero=(select numplan from personnel where numero ="+
                        personnel.Numero+")";
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            chemin = reader.GetString(0);
                        }
                    }
                    conn.Close();
                }
            }
            return chemin;
        }
    }
}
