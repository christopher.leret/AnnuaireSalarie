﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace annuairecorp
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            /*De base : Application.Run(new authentification());
              Modification pour quitter le form d'authentification
              lorsque la connexion est réaliser sans pour autant
              quitter l'application*/
            authentification LancementProg = new authentification();
            LancementProg.Show();
            Application.Run(); 
        }
    }
}
