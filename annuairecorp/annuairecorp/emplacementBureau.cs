﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace annuairecorp
{
    public partial class emplacementBureau : Form
    {
        personnel personnelRechercher = new personnel();
        database database = new database();

        public emplacementBureau(personnel personnel, database dbase)
        {
            InitializeComponent();
            personnelRechercher = personnel;
            database = dbase;
        }

        private void emplacementBureau_Load(object sender, EventArgs e)
        {
            recherchebureau rechercheBureau = new recherchebureau(database);
            rechercheBureau.infoPersonnel(personnelRechercher, lblIdentitePersonnel,
                lblCoordPersonnel, lblMetierPersonnel);
            Button btnEmplacementBureau = new Button();
            string coordSalle = database.coordonneeBatiment(personnelRechercher);
            int coordX = Convert.ToInt16(coordSalle.Split('_')[0]);
            int coordY = Convert.ToInt16(coordSalle.Split('_')[1]);
            int x = pictureBox1.Location.X + personnelRechercher.CoordX - coordX;
            int y = pictureBox1.Location.Y + personnelRechercher.CoordY - coordY;
            btnEmplacementBureau.SetBounds(x, y, 20, 20);
            btnEmplacementBureau.Enabled = false;
            this.Controls.Add(btnEmplacementBureau);
            btnEmplacementBureau.BringToFront();
            btnEmplacementBureau.BackColor = Color.Red;

            pictureBox1.ImageLocation="http://bts.bts-malraux72.net:6180/~c.leret/annuaireCorp/"+
                database.cheminPlan(personnelRechercher)+".jpg";
        }
    }
}
