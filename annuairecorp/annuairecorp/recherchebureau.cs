﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace annuairecorp
{
    public partial class recherchebureau : Form
    {
        private database database = new database();
        List<personnel> listePersonnel = new List<personnel>();
        personnel personnelRechercher = new personnel();
        Button btnEmplacementBureau = new Button();

        public recherchebureau(database database)
        {
            InitializeComponent();
            this.database = database;
        }

        private void recherchebureau_Load(object sender, EventArgs e)
        {
            btnRetour.Visible = false;
            implementCbbAlphabet();
            this.Controls.Add(btnEmplacementBureau);
            btnEmplacementBureau.BringToFront();
            btnEmplacementBureau.BackColor = Color.Red;
            btnEmplacementBureau.Visible = false;
            btnEmplacementBureau.Click += new EventHandler(btnEmplacementBureau_Click);
            panel1.Visible = false;
            lblErreurListe.Visible = false;
        }

        private void btnRechercher_Click(object sender, EventArgs e)
        {
            string lettre = cbbListe.Items[cbbListe.SelectedIndex].ToString();
            listePersonnel = database.listePersonnelParPremiereLettre(lettre);
            if (listePersonnel.Count > 0)
            {
                btnRechercher.Visible = false;
                btnRetour.Visible = true;
                cbbListe.Items.Clear();
                cbbListe.Text = "";
                cbbListe.Width = 140;
                lblRechResu.Text = "La liste des salaries correspondant :";

                foreach (personnel personnel in listePersonnel)
                {
                    cbbListe.Items.Add(personnel.Civilite + " " + personnel.Nom + " " + personnel.Prenom);
                }
                lblErreurListe.Visible = false;
            }
            else
            {
                lblErreurListe.Visible = true;
                lblErreurListe.Text = "Aucun personnel avec cette lettre";
            }
        }

        private void btnRetour_Click(object sender, EventArgs e)
        {
            btnRechercher.Visible = true;
            panel1.Visible = pictureBox1.Visible = btnEmplacementBureau.Visible = false;
            implementCbbAlphabet();
        }

        private void implementCbbAlphabet()
        {
            btnRetour.Visible = false;
            cbbListe.Items.Clear();
            cbbListe.Text = "";
            cbbListe.Width = 50;
            lblRechResu.Text = "Première lettre du prénom ou du nom :";
            List<String> alphabet = new List<string>{"a","b","c","d","e","f","g","h","i","j","k","l","m","n",
                                        "o","p","q","r","s","t","u","v","w","x","y","z"};
            foreach (String lettre in alphabet)
            {
                cbbListe.Items.Add(lettre);
            }
        }

        private void cbbListe_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (personnel personnel in listePersonnel)
            {
                String choixPersonnel = personnel.Civilite + " " + personnel.Nom + " " + personnel.Prenom;
                if ( cbbListe.Items[cbbListe.SelectedIndex].ToString().Equals(choixPersonnel))
                {
                    panel1.Visible = pictureBox1.Visible = true;
                    personnelRechercher = personnel;
                    infoPersonnel(personnel,lblIdentitePersonnel,lblCoordPersonnel,
                        lblMetierPersonnel);
                    int x = pictureBox1.Location.X + personnel.CoordX-10;
                    int y = pictureBox1.Location.Y + personnel.CoordY-10;
                    btnEmplacementBureau.SetBounds(x, y, 20, 20);
                    btnEmplacementBureau.Visible = true;
                    pictureBox1.ImageLocation = "http://bts.bts-malraux72.net:6180/~c.leret/annuaireCorp/general.jpg";
                }
            }
        }

        private void btnEmplacementBureau_Click(object sender, EventArgs e)
        {
            emplacementBureau formEmplacemnetBureau = new emplacementBureau(personnelRechercher, database);
            formEmplacemnetBureau.ShowDialog();
        }

        public void infoPersonnel(personnel personnel,Label lblIdentite, Label lblCoordonnee,
            Label lblMetier)
        {
            lblIdentite.Text = personnel.Civilite + " " + personnel.Nom + " " + personnel.Prenom;
            lblCoordonnee.Text = "Numéro de travail : " + personnel.NumTelTravail +
                "\nMail : " + personnel.MailTravail;
            lblMetier.Text = "Métier : " + personnel.Metier;
        }

        private void recherchebureau_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
