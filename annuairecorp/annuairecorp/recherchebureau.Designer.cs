﻿namespace annuairecorp
{
    partial class recherchebureau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRechercher = new System.Windows.Forms.Button();
            this.btnRetour = new System.Windows.Forms.Button();
            this.cbbListe = new System.Windows.Forms.ComboBox();
            this.lblRechResu = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCoordPersonnel = new System.Windows.Forms.Label();
            this.lblMetierPersonnel = new System.Windows.Forms.Label();
            this.lblIdentitePersonnel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblErreurListe = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRechercher
            // 
            this.btnRechercher.BackColor = System.Drawing.Color.Beige;
            this.btnRechercher.Location = new System.Drawing.Point(51, 92);
            this.btnRechercher.Name = "btnRechercher";
            this.btnRechercher.Size = new System.Drawing.Size(75, 23);
            this.btnRechercher.TabIndex = 0;
            this.btnRechercher.Text = "Rechercher";
            this.btnRechercher.UseVisualStyleBackColor = false;
            this.btnRechercher.Click += new System.EventHandler(this.btnRechercher_Click);
            // 
            // btnRetour
            // 
            this.btnRetour.BackColor = System.Drawing.Color.Beige;
            this.btnRetour.Location = new System.Drawing.Point(146, 92);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(75, 23);
            this.btnRetour.TabIndex = 1;
            this.btnRetour.Text = "Retour";
            this.btnRetour.UseVisualStyleBackColor = false;
            this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
            // 
            // cbbListe
            // 
            this.cbbListe.FormattingEnabled = true;
            this.cbbListe.Location = new System.Drawing.Point(215, 37);
            this.cbbListe.Name = "cbbListe";
            this.cbbListe.Size = new System.Drawing.Size(50, 21);
            this.cbbListe.TabIndex = 2;
            this.cbbListe.SelectedIndexChanged += new System.EventHandler(this.cbbListe_SelectedIndexChanged);
            // 
            // lblRechResu
            // 
            this.lblRechResu.AutoSize = true;
            this.lblRechResu.Location = new System.Drawing.Point(12, 41);
            this.lblRechResu.Name = "lblRechResu";
            this.lblRechResu.Size = new System.Drawing.Size(186, 13);
            this.lblRechResu.TabIndex = 3;
            this.lblRechResu.Text = "Première lettre du prénom ou du nom :";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblCoordPersonnel);
            this.panel1.Controls.Add(this.lblMetierPersonnel);
            this.panel1.Controls.Add(this.lblIdentitePersonnel);
            this.panel1.Location = new System.Drawing.Point(369, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(271, 109);
            this.panel1.TabIndex = 4;
            // 
            // lblCoordPersonnel
            // 
            this.lblCoordPersonnel.AutoSize = true;
            this.lblCoordPersonnel.Location = new System.Drawing.Point(4, 28);
            this.lblCoordPersonnel.Name = "lblCoordPersonnel";
            this.lblCoordPersonnel.Size = new System.Drawing.Size(16, 13);
            this.lblCoordPersonnel.TabIndex = 6;
            this.lblCoordPersonnel.Text = "...";
            // 
            // lblMetierPersonnel
            // 
            this.lblMetierPersonnel.AutoSize = true;
            this.lblMetierPersonnel.Location = new System.Drawing.Point(4, 61);
            this.lblMetierPersonnel.Name = "lblMetierPersonnel";
            this.lblMetierPersonnel.Size = new System.Drawing.Size(16, 13);
            this.lblMetierPersonnel.TabIndex = 1;
            this.lblMetierPersonnel.Text = "...";
            // 
            // lblIdentitePersonnel
            // 
            this.lblIdentitePersonnel.AutoSize = true;
            this.lblIdentitePersonnel.Location = new System.Drawing.Point(4, 7);
            this.lblIdentitePersonnel.Name = "lblIdentitePersonnel";
            this.lblIdentitePersonnel.Size = new System.Drawing.Size(16, 13);
            this.lblIdentitePersonnel.TabIndex = 0;
            this.lblIdentitePersonnel.Text = "...";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(109, 167);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(461, 362);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // lblErreurListe
            // 
            this.lblErreurListe.AutoSize = true;
            this.lblErreurListe.ForeColor = System.Drawing.Color.Red;
            this.lblErreurListe.Location = new System.Drawing.Point(26, 65);
            this.lblErreurListe.Name = "lblErreurListe";
            this.lblErreurListe.Size = new System.Drawing.Size(13, 13);
            this.lblErreurListe.TabIndex = 6;
            this.lblErreurListe.Text = "..";
            // 
            // recherchebureau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(684, 541);
            this.Controls.Add(this.lblErreurListe);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblRechResu);
            this.Controls.Add(this.cbbListe);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.btnRechercher);
            this.Name = "recherchebureau";
            this.Text = "Recherche bureau par personnel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.recherchebureau_FormClosing);
            this.Load += new System.EventHandler(this.recherchebureau_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRechercher;
        private System.Windows.Forms.Button btnRetour;
        private System.Windows.Forms.ComboBox cbbListe;
        private System.Windows.Forms.Label lblRechResu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblIdentitePersonnel;
        private System.Windows.Forms.Label lblMetierPersonnel;
        private System.Windows.Forms.Label lblCoordPersonnel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblErreurListe;
    }
}